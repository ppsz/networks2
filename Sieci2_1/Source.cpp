#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <utility>

const int g_packet_length = 18;

struct Header
{
	unsigned int number;
	unsigned int length;
};


std::string g_extension = ".txt";
std::string g_input_file_name = "input" + g_extension;
std::string g_output_file_name = "output" + g_extension;

int main()
{
	std::ifstream input_file;
	std::ofstream output_file;
	std::string input_file_name, output_file_name;
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(0, 1);
	int number_of_packets, packet_length_remainder;
	int file_position = g_packet_length;
	int packet_length = g_packet_length;
	unsigned long long file_length;
	char* packet;
	Header* header;
	Header** packets;
	input_file_name = g_input_file_name;
	output_file_name = g_output_file_name;

	// Opening files
	input_file.open(input_file_name, std::ios::in | std::ios::binary);
	if (!input_file.is_open()) std::cout << "Error while opening file: " << input_file_name;

	output_file.open(output_file_name, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!output_file.is_open()) std::cout << "Error while opening file: " << output_file_name;
	output_file.write("", 0);
	output_file.close();

	// Taking length of the file
	input_file.seekg(0, input_file.end);
	file_length = input_file.tellg();
	input_file.seekg(0, input_file.beg);
	input_file.close();

	// Creating buffer
	number_of_packets = std::floor(file_length / packet_length);
	if (file_length % packet_length != 0) ++number_of_packets;
	packet_length_remainder = file_length % packet_length;
	packets = new Header*[number_of_packets];

	for (unsigned int i = 0; i < number_of_packets; ++i)
	{
		if (i == (number_of_packets - 1) && packet_length_remainder > 0 && packet_length_remainder != packet_length)
		{
			packet_length = packet_length_remainder;
		}
		else if (i == (number_of_packets - 1) && packet_length_remainder == 0)
		{
			break;
		}

		packet = new char[sizeof(Header) + packet_length];
		header = reinterpret_cast<Header*>(packet);
		header->number = i;
		header->length = packet_length;

		// Reading from file to buffer
		input_file.open(input_file_name, std::ios::in | std::ios::binary);
		if (!input_file.is_open()) std::cout << "Error while opening file: " << input_file_name;
		// Changing reading position
		input_file.seekg((file_position*i), std::ios::cur);

		input_file.read((packet + sizeof(Header)), packet_length);
		if (!input_file) std::cout << "Error, only: " << input_file.gcount() << " chars could be read\n";
		input_file.close();

		packets[i] = header;
	}

	//Shuffle
	for (int i = 1; i < number_of_packets; ++i)
	{
		int random = dist(mt);
		if (random == 1)
			std::swap(packets[i], packets[i - 1]);
	}

	//Sort
	for (int i = 0; i<number_of_packets - 1; ++i)
		for (int j = 0; j<number_of_packets - 1 - i; ++j)
			if (packets[j]->number > packets[j + 1]->number)
				std::swap(packets[j], packets[j + 1]);

	for (int i = 0; i < number_of_packets; ++i)
	{
		output_file.open(output_file_name, std::ios::in | std::ios::binary | std::ios::ate);
		if (!output_file.is_open()) std::cout << "Error while opening file: " << output_file_name;
		output_file.write(reinterpret_cast<char*>(packets[i]), packets[i]->length + sizeof(Header));
		output_file.close();
	}

	system("pause");
}